"""
core of argparse
"""
import argparse


class NestedNamespace(argparse.Namespace):
    """
    Inspired by
        https://stackoverflow.com/questions/18668227/
            argparse-subcommands-with-nested-namespaces
    Examples:
        >>> import argparse
        >>> p = argparse.ArgumentParser()
        >>> _ = p.add_argument('--foo')
        >>> _ = p.add_argument('--bar', dest='test.bar')
        >>> _ = p.add_argument('--test.deep.doo', dest='test.deep.doo')
        >>> _ = p.add_argument('--test.deep.dat')
        >>> args = p.parse_args(("--foo fooValue --bar barValue "
        ...                      "--test.deep.doo dooValue "
        ...                      "--test.deep.dat datValue").split(),
        ...                     NestedNamespace())
        >>> isinstance(args, NestedNamespace)
        True
        >>> args.foo == 'fooValue'
        True
        >>> print(args.test)
        NestedNamespace(bar='barValue', deep=NestedNamespace(dat='datValue', doo='dooValue'))
        >>> print(args.test.deep)
        NestedNamespace(dat='datValue', doo='dooValue')

        One example use is the following with using the 'get_dict' method:
        >>> class C(object):
        ...     def __init__(self, bar=None, deep=None):
        ...         self.bar = bar
        ...         self.deep = deep
        >>> obj = C(**args.test.get_dict())
        >>> print(obj.bar)
        barValue

    """
    def __setattr__(self, name, value):
        if '.' in name:
            group, name = name.split('.', 1)
            ns = getattr(self, group, NestedNamespace())
            setattr(ns, name, value)
            self.__dict__[group] = ns
        else:
            self.__dict__[name] = value

    def __getattr__(self, name):
        """
        needed for actions like count and append
        """
        if '.' in name:
            group, name = name.split('.', 1)
            try:
                ns = self.__dict__[group]
            except KeyError:
                raise AttributeError
            return getattr(ns, name)
        else:
            raise AttributeError

    def get_dict(self):
        """
        Returns nested dict representing the NestedNamespace
        """
        return {key: (value if not isinstance(value, self.__class__)
                      else value.get_dict())
                for key, value in vars(self).items()}


class SParser(argparse.ArgumentParser):
    """
    Super ArgumentParser that provides some default options.
    """
    def __init__(self, *args, **kwargs):
        super(SParser, self).__init__(*args, **kwargs)
        self.add_argument('--config', '-c',
                          help=("Specify a config file from which to retrieve"
                                "the argumetns."))
        self._default_file = None

    @property
    def default_file(self):
        """
        (str): The path to the default file
        """
        return self._default_file

    @default_file.setter
    def default_file(self, path):
        self._default_file = path

    def parse_args(self, args=None, namespace=None):
        if namespace is None:
            namespace = NestedNamespace()
        return super(SParser, self).parse_args(args, namespace)


if __name__ == '__main__':
    import doctest
    doctest.testmod()
